#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Time series analysis of plasma hormones and inflammatory cytokines following heart surgery
Created on Mon Oct 5 14:08:19 2020
@author: Eder Zavala
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1.inset_locator import (inset_axes,InsetPosition,mark_inset)
import matplotlib.dates as mdates
import seaborn as sns
import glob, os
import scipy.stats as stats
from scipy.signal import hilbert, butter, filtfilt, find_peaks
from scipy.fftpack import fft,fftfreq,rfft,irfft,ifft
from statsmodels.graphics.tsaplots import plot_acf
from scipy.integrate import simps

# Plot settings
%config InlineBackend.figure_format = 'retina'
%matplotlib inline
plt.rcParams.update({'font.size': 15})
def fmt_xaxes(ax):
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.tick_params(labelsize='small')
    ax.set_xlim(0,max(time))
    ax.set_xticks([12,24,36,48,60,72])
    ticks_x = ticker.FuncFormatter(lambda time, pos: '{0:g}'.format(10*time/60))
    ax.xaxis.set_major_formatter(ticks_x)

def fmt_event(ax):
    # Surgery time
    ax.axvline(KTS,linestyle='--',color='k',lw=1.5,alpha=0.3)
    ax.axvline(SC,linestyle='--',color='k',lw=1.5,alpha=0.3)
    ax.axvspan(KTS,SC,facecolor='k',alpha=0.05)
    # Bypass time, if present
    ax.axvline(ByON,linestyle='--',color='r',lw=1.5,alpha=0.3)
    ax.axvline(ByOFF,linestyle='--',color='r',lw=1.5,alpha=0.3)
    ax.axvspan(ByON,ByOFF,facecolor='r',alpha=0.05)

xa_color = 'tab:blue'
ya_color = 'indianred'

# xa_color = 'peru'
# ya_color = 'olivedrab'
# ya_color = 'purple'
# ya_color = 'teal'

# xa_color = 'tab:blue'
# xa_color = 'indianred'
# ya_color = 'peru'
# ya_color = 'olivedrab'
# ya_color = 'purple'
# ya_color = 'teal'

# Save plots and output files?
save = 'n' # y/n

#%% FILE PATH

# Select whether you're working on workstation (Admin) or laptop (ederzavala)
comp = 'Admin'
# comp = 'ederzavala'

# Set paths
parent = '/Users/'+comp+'/Dropbox/Project - Heart surgery inflammation (Ben Gibbison)'
data_path = parent+'/data'
save_path = parent+'/results/patients'

# Change directory
os.chdir(data_path)

#%% ANALYSIS SETTINGS

# Select hormone of interest from the list:
# ['IL10', 'IL-1a', 'IL-4', 'IL-6', 'IL-8', 'TNFa', 'ACTH', 'Cortisol']

x_analyte = 'ACTH'
y_analyte = 'Cortisol'

# x_analyte = 'IL-6'
# y_analyte = 'IL-8'
# y_analyte = 'IL10'
# y_analyte = 'TNFa'

# x_analyte = 'Cortisol'
# y_analyte = 'IL-6'
# y_analyte = 'IL-8'
# y_analyte = 'IL10'
# y_analyte = 'TNFa'

print()
print('RUNNING...')
print('Phase synchrony analysis of pairs of plasma analytes (heart surgery & inflammation project)')
print()
print('Comparing '+x_analyte+' vs '+y_analyte)

#%% LOAD DATA

# Read auxiliary file
st = pd.read_excel('surgery_times.xlsx')

# Some aux calculations
PID = []
for filename in os.listdir(data_path):
    if filename.endswith('.csv'):
        PID.append(filename.split(".")[0])

# Read data files
filelist=glob.glob('2*.csv')
print()
print('Available data files: ', filelist)

#%% MAIN LOOP

y1_AUC = []
y2_AUC = []
# Loop over all files (slower) or select a single file to analyse
# Available data files:  ['2094.csv', '2095.csv', '2097.csv', '2120.csv', '2079.csv','2102.csv', '2075.csv', '2101.csv', '2110.csv', '2098.csv']
# for i in range(1):  # To select a single file, uncomment this line and the following
    # file = '2094.csv'
for file in filelist:  # To select a single file, comment this line

#%% LOAD DATA

    # Read csv file into a dataframe, create a sub-dataframe with the hormones to compare
    data_df=pd.read_csv(file, parse_dates=True, index_col=['Time point'], header=0, dayfirst=True)
    print()
    print('------------------------------')
    print('Analysing: ' +str(file))
    print('------------------------------')

    # Select two hormones to compare (usually plasma vs MD versions)
    df = data_df[[x_analyte, y_analyte]].copy()
    time = df.index

    # Set events
    for j in range(len(st)):
        if file.split('.')[0]==str(int(st.iloc[j].PID)):
            KTS   = st.iloc[j].KTS/10
            SC    = st.iloc[j].SC/10
            ByON  = st.iloc[j].ByON/10
            ByOFF = st.iloc[j].ByOFF/10

#%% CALCULATIONS

    # Plasma/MD ratios
    df_ratio = df[x_analyte]/df[y_analyte].dropna()

    # Area Under the Curve
    y1 = np.array(df[x_analyte].dropna())
    y2 = np.array(df[y_analyte].dropna())
    y1_auc = simps(y1, dx=0.16666)  # dx is interval length of 10 min measured in hours
    y2_auc = simps(y2, dx=0.16666)  # dx is interval length of 10 min measured in hours
    y1_AUC.append(y1_auc)
    y2_AUC.append(y2_auc)
    auc_ratio = y1_auc/y2_auc
    print()
    print(f"Simpson's AUC for {x_analyte} =",y1_auc)
    print(f"Simpson's AUC for {y_analyte} =",y2_auc)
    print(f"AUC ratio {x_analyte}/{y_analyte} =",auc_ratio)

    window_size = 6  # Time step is 10 min so a window_size=6 equals 1 hr

    # Zscores
    df_zscore = (df - df.mean())/df.std() #standardise the data in each column

    # Zscore ratios
    df_zratio = (df_zscore[x_analyte]/df_zscore[y_analyte].dropna())

    # Zscore rolling mean
    df_zrm = df_zscore.rolling(window_size, center=True, win_type='gaussian').mean(std=6)

    # Compute rolling window synchrony
    rolling_corr = df_zrm[x_analyte].rolling(window=window_size, center=True).corr(df_zrm[y_analyte])

    # 1st order discrete differentiation of Z-score
    df_zdiff = df_zscore.diff()

    # Spearman correlation coefficient and p-value
    r, p = stats.spearmanr(df, nan_policy='omit')
    print()
    print(f"Spearman r = {r} and p-value = {p}")

    # Spearman correlation of first order differentials
    sc1d = df.diff().corr(method='spearman')
    print()
    print(f"Spearman correlation of 1st order differentials:")
    print(f"{sc1d}")

    # Time lagged cross correlation
    def crosscorr(datax, datay, lag=0, wrap=False, method='spearman'):  # Default values. <lag> is a range centered at zero.
        """ Lag-N cross correlation.
        Shifted data filled with NaNs

        Parameters
        ----------
        lag : int, default 0
        datax, datay : pandas.Series objects of equal length

        Returns
        ----------
        crosscorr : float
        """
        if wrap:
            shiftedy = datay.shift(lag)
            shiftedy.iloc[:lag] = datay.iloc[-lag:].values
            return datax.corr(shiftedy)
            print(datax.corr(shiftedy))
        else:
            return datax.corr(datay.shift(lag), method='spearman')

    # Butterworth bandpass filter
    def butter_bandpass(lowcut, highcut, fs, order=5):
        nyq = 0.5 * fs
        low = lowcut / nyq
        high = highcut / nyq
        b, a = butter(order, [low, high], btype='band')
        return b, a

    def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
        b, a = butter_bandpass(lowcut, highcut, fs, order=order)
        y = filtfilt(b, a, data)
        return y
    # Sample rate and desired cutoff frequencies (in Hz)
    fs = 3.
    lowcut  = .01
    highcut = .5
    order = 1

    # Fourier spectral analysis
    df_fft = fft(df)  # Fast Fourier Transform
    df_psd = np.abs(df_fft)**2  # Power Spectrum Density
    natfreqs = fftfreq(len(df_psd), 1)  # Extract frequencies from the PSD
    j = natfreqs > 0  # Keep positive elements only

    # Find frequency peaks using FFT
    # max_freq = natfreqs[np.argmax(np.log10(df_psd[:,0]))]  # Using x_analyte to calculate this
    peaks,_ = find_peaks(np.log10(df_psd[:,0]),prominence=0.1)  # Using x_analyte to calculate this
    loc_max = natfreqs[peaks]
    x = loc_max > 0  # Keep positive elements only
    # loc_max = loc_max[x]
    max_freq = max(loc_max[x])

    # T_max = 1/loc_max[np.where(loc_max == max_freq)[0][0]-1]
    # T_min = 1/loc_max[np.where(loc_max == max_freq)[0][0]+1]
    # T_min2 = 1/loc_max[np.where(loc_max == max_freq)[0][0]+2]

    # Recover original signal using FFT
    df_fft_bis = df_fft.copy()
    df_slow = np.real(ifft(df_fft_bis))  # Inverse FFT

    # Filter freqs from original signal
    df_fft_bis[np.abs(natfreqs) > max_freq] = 0  # Cut frequencies higher than the fundamental frequency
    df_slow_bis = np.real(ifft(df_fft_bis))  # Inverse FFT

#%% PLOT: Interpolated data

    fig1 = plt.figure(1, facecolor='white')
    plt.clf()

    ax1 = fig1.add_subplot(211)
    ax2 = fig1.add_subplot(212,sharex=ax1)

    ax1.plot(df.index,df[x_analyte],'-',lw=2,color=xa_color,label=x_analyte)
    ax2.plot(df.index,df[y_analyte],'--',lw=2,color=ya_color,label=y_analyte)

    ax1.set_xlabel('')
    ax2.set_xlabel('')
    if x_analyte=='ACTH':
        ax1.set_ylabel(x_analyte+' (ng/L)')
    elif x_analyte=='Cortisol':
        ax1.set_ylabel(x_analyte+' (nM)')
    else:
        ax1.set_ylabel(x_analyte+' (pg/mL)')
    if y_analyte=='ACTH':
        ax2.set_ylabel(y_analyte+' (ng/L)')
    elif y_analyte=='Cortisol':
        ax2.set_ylabel(y_analyte+' (nM)')
    else:
        ax2.set_ylabel(y_analyte+' (pg/mL)')
    ax1.set_xlim(min(df.index),max(df.index))
    fmt_xaxes(ax1)
    fmt_xaxes(ax2)
    fmt_event(ax1)
    fmt_event(ax2)

    plt.xlabel('Time (hr)')
    # plt.text(-0.12, -0.4, 'Source: '+str(file), transform=ax2.transAxes, fontsize=6)

    fig1.tight_layout()
    plt.show()

#%% PLOT: Ratio of x_analyte:y_analyte

    fig2 = plt.figure(2, facecolor='white')
    plt.clf()

    ax1 = fig2.add_subplot(111)

    ax2 = plt.axes([0,0,1,1])    # Create a set of inset Axes: these should fill the bounding box allocated to them
    ip = InsetPosition(ax1,[0.9,0.0,0.3,1.0])    # Manually set the position and relative size of the inset axes within ax1
    ax2.set_axes_locator(ip)

    ax1.plot(df.index,df_ratio,'-',lw=2,color='k',label='Ratio')
    ax2 = df_ratio.plot.box(label='')
    ax2.axis('off')

    ax1.set_xlabel('Time (hr)')
    ax1.set_ylabel(x_analyte+'/'+y_analyte)
    ax1.set_xlim(min(df.index),max(df.index))
    ax1.set_ylim(0.02,1.3)
    ax2.set_ylim(0.02,1.3)
    ax1.legend(frameon=False,fontsize=10,loc='upper right')
    fmt_xaxes(ax1)
    fmt_event(ax1)

    plt.text(0.8, 0.1, r'$\mu \pm \sigma = $'+str(round(df_ratio.mean(),2))+r' $\pm$ '+str(round(df_ratio.std(),2)), transform=ax2.transAxes, fontsize=10, rotation=90)
    # plt.text(-0.12, -0.17, 'Source: '+str(file), transform=ax1.transAxes, fontsize=6)

    fig2.tight_layout()
    plt.show()

#%% PLOT: Zscores

    fig3 = plt.figure(3, facecolor='white')
    plt.clf()

    ax1 = fig3.add_subplot(111)

    ax1.plot(df.index,df_zscore[x_analyte],'-',lw=2,color=xa_color,label=x_analyte)
    ax1.plot(df.index,df_zscore[y_analyte],'--',lw=2,color=ya_color,label=y_analyte)

    ax1.set_xlabel('')
    ax1.set_ylabel('Z-score')
    ax1.set_xlim(min(df.index),max(df.index))
    ax1.legend(frameon=False,fontsize=10,loc='upper left')
    fmt_xaxes(ax1)
    fmt_event(ax1)

    plt.xlabel('Time (hr)')
    # plt.text(-0.12, -0.17, 'Source: '+str(file), transform=ax1.transAxes, fontsize=6)

    fig3.tight_layout()
    plt.show()

#%% PLOT: Rolling mean

    fig4 = plt.figure(4, facecolor='white')
    plt.clf()

    ax1 = fig4.add_subplot(211)
    ax2 = fig4.add_subplot(212,sharex=ax1)

    ax1.plot(df.index,df_zrm[x_analyte],'-',lw=2,color=xa_color,label=x_analyte)
    ax1.plot(df.index,df_zrm[y_analyte],'--',lw=2,color=ya_color,label=y_analyte)
    ax2.plot(df.index,rolling_corr,'-',lw=2,color='k',label='')

    ax1.set_xlabel('')
    ax2.set_xlabel('')
    ax1.set_ylabel('Z-score ($\overline{\mu}$) \n $_{RW='+str(window_size*10)+' min}$')
    ax2.set_ylabel(r'Pearson $r_{X,Y}$')
    ax1.set_xlim(min(df.index),max(df.index))
    ax2.set_ylim(-1.1,1.1)
    ax1.legend(frameon=False,fontsize=10,loc='upper left')
    fmt_xaxes(ax1)
    fmt_xaxes(ax2)
    fmt_event(ax1)
    fmt_event(ax2)

    plt.xlabel('Time (hr)')
    # plt.text(-0.15, -0.4, 'Source: '+str(file), transform=ax2.transAxes, fontsize=6)

    fig4.tight_layout()
    plt.show()

#%% PLOT: 1st-order diff

    fig5 = plt.figure(5, facecolor='white')
    plt.clf()

    ax1 = fig5.add_subplot(111)

    # First order differencing (discrete derivative) to remove dirunal trend
    ax1.plot(df.index,df_zdiff[x_analyte],'-',lw=2,color=xa_color,label=x_analyte)
    ax1.plot(df.index,df_zdiff[y_analyte],'--',lw=2,color=ya_color,label=y_analyte)
    ax1.hlines(0,min(df.index),max(df.index),lw=1,color='k',linestyle='--')

    ax1.set_xlabel('')
    ax1.set_ylabel('1st(O) diff Z-score')
    ax1.set_xlim(min(df.index),max(df.index))
    ax1.legend(frameon=False,fontsize=10,loc='upper left')
    fmt_xaxes(ax1)
    fmt_event(ax1)

    plt.xlabel('Time (hr)')
    # plt.text(-0.18, -0.17, 'Source: '+str(file), transform=ax1.transAxes, fontsize=6)

    fig5.tight_layout()
    plt.show()

#%% PLOT: Autocorrelations

    fig6 = plt.figure(6, facecolor='white')
    plt.clf()

    ax1 = fig6.add_subplot(211)
    ax2 = fig6.add_subplot(212,sharex=ax1)

    plot_acf(df[x_analyte].values,ax=ax1,missing='conservative',title='',label=x_analyte)
    plot_acf(df[y_analyte].values,ax=ax2,missing='conservative',title='',label=y_analyte)

    ax1.set_ylabel(x_analyte)
    ax2.set_ylabel(y_analyte)
    ax1.tick_params(labelbottom=False)
    plt.xlabel('Lags (1 lag = 10 min)')
    plt.text(0.7, 0.7, '95% confidence cone', transform=ax2.transAxes, fontsize=8)
    # plt.text(-0.09, -0.4, 'Source: '+str(file), transform=ax2.transAxes, fontsize=6)

    fig6.tight_layout()
    plt.show()

#%% PLOT: Time lagged cross correlation to find peak synchronicity (assumes 10 minute interpolation)

    fig7 = plt.figure(7, facecolor='white')
    plt.clf()

    ax1 = fig7.add_subplot(111)

    # Time lagged cross correlation (tlcc)
    # Set number of samples to shift in each direction
    samples = 18  # Time step is 10 min so sample=6 equals 1 hr
    s_space = range(-samples,samples+1) # Sampling space, centered at 0 shift
    datax = df.dropna()[x_analyte]
    datay = df.dropna()[y_analyte]
    tlcc = [crosscorr(datax,datay,lag) for lag in s_space]

    centretick = s_space.index(0)
    peaktick = np.argmax(tlcc)
    offset = (abs(centretick-peaktick))*10

    ax1.plot(tlcc)
    ax1.axvline((centretick),color='k',linestyle='--',label='Centre')
    ax1.axvline(peaktick,color='r',linestyle='--',label='Peak sync')


    ticks = range(0,int(len(s_space)),6)
    ticklabels = range(-(len(s_space)//(2*6)),(len(s_space)//(2*6))+1,1)
    ax1.set_xticks(ticks)
    ax1.set_xticklabels(ticklabels)
    ax1.set_xlabel('Time lag (hr)')
    ax1.set_ylabel('Spearman r')
    ax1.legend(frameon=False,fontsize=10,loc='upper right')

    if centretick > peaktick:
        plt.text(0.05, 0.85, x_analyte+' leads \n by '+str(offset)+' min', transform=ax1.transAxes, fontsize=10)
    else:
        plt.text(0.05, 0.85, y_analyte+' leads \n by '+str(offset)+' min', transform=ax1.transAxes, fontsize=10)
    # plt.text(-0.2, -0.17, 'Source: '+str(file), transform=ax1.transAxes, fontsize=6)

    fig7.tight_layout()
    plt.show()

#%% PLOT: Windowed time lagged cross correlation

    fig8 = plt.figure(8, facecolor='white', figsize=(10,8))
    plt.clf()

    ax1 = fig8.add_subplot(111)

    # Windowed time lagged cross correlation (wtlcc)
    # Set number of samples to shift in each direction
    samples = 18  # Time step is 10 min so sample=6 equals 1 hr
    s_space = range(-samples,samples+1) # Sampling space, centered at 0 shift
    centretick = s_space.index(0)
    splits = 4
    samples_per_split = int(len(df.dropna())/splits)
    wtlcc=[]
    for t in range(0, splits):
        datax = df.dropna()[x_analyte].iloc[(t)*samples_per_split:(t+1)*samples_per_split]
        datay = df.dropna()[y_analyte].iloc[(t)*samples_per_split:(t+1)*samples_per_split]
        tlcc = [crosscorr(datax,datay,lag) for lag in s_space]
        wtlcc.append(tlcc)
    wtlcc = pd.DataFrame(wtlcc)

    ax1 = sns.heatmap(wtlcc,cmap='RdBu_r')
    cbar = ax1.collections[0].colorbar
    cbar.ax.set_ylabel('WTLCC')
    ax1.axvline((centretick),color='w',linestyle='--',lw=3,label='Centre')

    ticks = range(0,int(len(s_space)),6)
    ticklabels = range(-(len(s_space)//(2*6)),(len(s_space)//(2*6))+1,1)
    ax1.set_xticks(ticks)
    ax1.set_xticklabels(ticklabels)
    ax1.set_xlabel('Time lag (hr)')
    ax1.set_ylabel('Window epochs')

    # plt.text(-0.07, -0.08, 'Source: '+str(file), transform=ax1.transAxes, fontsize=6)

    fig8.tight_layout()
    plt.show()

#%% PLOT: Rolling window time lagged cross correlation

    fig9 = plt.figure(9, facecolor='white', figsize=(10,8))
    plt.clf()

    ax1 = fig9.add_subplot(111)

    # Rolling windowed time lagged cross correlation (wtlcc)
    # Set number of samples to shift in each direction
    samples = int(len(df)/2)  # Time step is 10 min so sample=6 equals 1 hr
    s_space = range(-samples,samples+1) # Sampling space, centered at 0 shift
    centretick = s_space.index(0)

    window_size = samples #determines the number of epochs
    t_start = 0
    t_end = t_start + window_size
    step_size = 1
    rwtlcc=[]
    while t_end < int(len(df.dropna())):
        datax = df.dropna()[x_analyte].iloc[t_start:t_end]
        datay = df.dropna()[y_analyte].iloc[t_start:t_end]
        tlcc = [crosscorr(datax,datay,lag) for lag in s_space]
        rwtlcc.append(tlcc)
        t_start = t_start + step_size
        t_end = t_end + step_size
    rwtlcc = pd.DataFrame(rwtlcc)

    ax1 = sns.heatmap(rwtlcc,cmap='RdBu_r')
    cbar = ax1.collections[0].colorbar
    cbar.ax.set_ylabel('RWTLCC')
    ax1.axvline((centretick),color='w',linestyle='--',lw=3,label='Centre')

    ticks = range(0,int(len(s_space)),6)
    ticklabels = range(-(len(s_space)//(2*6)),(len(s_space)//(2*6))+1,1)
    ax1.set_xticks(ticks)
    ax1.set_xticklabels(ticklabels)

    ax1.set_xlabel('Time lag (hr)')
    ax1.set_ylabel('Epochs')

    # plt.text(-0.07, -0.08, 'Source: '+str(file), transform=ax1.transAxes, fontsize=6)

    fig9.tight_layout()
    plt.show()

#%% PLOT: Instantaneous Phase Synchrony

    fig10 = plt.figure(10, facecolor='white', figsize=(10,8))
    plt.clf()

    ax1 = fig10.add_subplot(411)
    ax2 = fig10.add_subplot(412,sharex=ax1)
    ax3 = fig10.add_subplot(413,sharex=ax1)
    ax4 = fig10.add_subplot(414,sharex=ax1)

    #  Apply Butterworth filter, Hilbert transform, and calculate phase synchrony
    datax = df.dropna()[x_analyte]
    datay = df.dropna()[y_analyte]
    c1 = butter_bandpass_filter(datax,lowcut=lowcut,highcut=highcut,fs=fs,order=order)
    c2 = butter_bandpass_filter(datay,lowcut=lowcut,highcut=highcut,fs=fs,order=order)
    al1 = np.angle(hilbert(c1),deg=False)
    al2 = np.angle(hilbert(c2),deg=False)
    phase_synchrony = 1-np.sin(np.abs(al1-al2)/2)
    N = len(al1)

    # Plot filtered data
    ax1.plot(datax.index,c1,'-',lw=2,color=xa_color,label=x_analyte)
    ax2.plot(datax.index,c2,'--',lw=2,color=ya_color,label=y_analyte)

    # Plot angle
    ax3.plot(datax.index,al1,'-',lw=2,color=xa_color,label=x_analyte)
    ax3.plot(datax.index,al2,'--',lw=2,color=ya_color,label=y_analyte)

    # Plot phase synchrony
    ax4.plot(datax.index,phase_synchrony,'-',lw=2,color='k')

    ax1.set_ylabel(x_analyte)
    ax2.set_ylabel(y_analyte)
    ax3.set_ylabel('Angle')
    ax4.set_ylabel('IPS')
    ax1.set_xlim(min(datax.index),max(datax.index))
    fmt_xaxes(ax1)
    fmt_xaxes(ax2)
    fmt_xaxes(ax3)
    fmt_xaxes(ax4)
    fmt_event(ax1)
    fmt_event(ax2)
    fmt_event(ax3)
    fmt_event(ax4)

    plt.xlabel('Time (hr)')
    # plt.text(-0.1, -0.4, 'Source: '+str(file), transform=ax4.transAxes, fontsize=6)

    fig10.tight_layout()
    plt.show()

#%% PLOT: FFT

    fig11 = plt.figure(11, facecolor='white', figsize=(10,7))
    plt.clf()

    ax1 = fig11.add_subplot(311)
    ax2 = fig11.add_subplot(312)
    ax2b = ax2.twinx()
    ax3 = fig11.add_subplot(313)
    ax3b = ax3.twinx()

    ax1.plot(natfreqs[j],10*np.log10(df_psd[j,0]),'-',lw=2,color=xa_color,label=x_analyte)
    ax1.plot(natfreqs[j],10*np.log10(df_psd[j,1]),'--',lw=2,color=ya_color,label=y_analyte)
    # for i in loc_max:
        # ax1.axvline(i, color='k', linestyle='--',lw=1)
    ax1.axvline(max_freq, color='r', linestyle='--',lw=2)
    # ax1.axvline(1/T_max, color='k', linestyle='--',lw=1)
    # ax1.axvline(1/T_min, color='k', linestyle='--',lw=1)
    # ax1.axvline(1/T_min2, color='k', linestyle='--',lw=1)
    ax1.set_xlabel('Frequency (hr$^{-1}$)')
    ax1.set_ylabel('PSD (dB)')
    ax1.spines['top'].set_visible(False)
    ax1.spines['right'].set_visible(False)
    ax1.tick_params(labelsize='small')
    ax1.legend(frameon=False,fontsize=10,loc='best')
    ax1.text(0.82, 0.4, 'F$_{max}$ = '+str(round(max_freq,2))+' hr$^{-1}$', transform=ax1.transAxes, fontsize=10)

    ax2.plot(df.index,df_slow[:,0],'-',lw=2,color=xa_color,label=x_analyte)
    ax2b.plot(df.index,df_slow[:,1],'--',lw=2,color=ya_color,label=y_analyte)
    ax2.set_ylabel(x_analyte+' iFFT')
    ax2.set_xlim(min(df.index),max(df.index))
    ax2b.set_ylabel(y_analyte+' iFFT')
    fmt_xaxes(ax2)
    fmt_xaxes(ax2b)
    ax2b.spines['right'].set_visible(True)
    # ax2.text(0.8, 0.5, 'Inverse FFT \nT = '+str(round(1/max_freq,2))+' hr at F$_{max}$ \nT$_{lo}$ = '+str(round(T_min,2))+' hr\nT$_{lo2}$ = '+str(round(T_min2,2))+' hr\nT$_{hi}$ = '+str(round(T_max,2))+' hr', transform=ax2.transAxes, fontsize=10)
    ax2.text(0.82, 0.1, 'Inverse FFT \nT = '+str(round(1/max_freq,2))+' hr at F$_{max}$', transform=ax2.transAxes, fontsize=10)

    ax3.plot(df.index,df_slow_bis[:,0],'-',lw=2,color=xa_color,label=x_analyte)
    ax3b.plot(df.index,df_slow_bis[:,1],'--',lw=2,color=ya_color,label=y_analyte)
    ax3.set_xlabel('Clock time (hr)')
    ax3.set_ylabel(x_analyte+' iFFT')
    ax3.set_xlim(min(df.index),max(df.index))
    ax3b.set_ylabel(y_analyte+' iFFT')
    fmt_xaxes(ax3)
    fmt_xaxes(ax3b)
    ax3b.spines['right'].set_visible(True)
    ax3.text(0.82, 0.1, 'Lo-pass cut \nT < '+str(round(1/max_freq,2))+' hr', transform=ax3.transAxes, fontsize=10)
    # ax3.text(-0.07, -0.3, 'Source: '+str(file), transform=ax3.transAxes, fontsize=6)

    fig11.tight_layout(h_pad=0)
    plt.show()

#%% SAVE FIGURES AND OUTPUT FILES

    os.chdir(save_path)
    if save=='y':
        fig1.savefig(str(file)+ f' {x_analyte} vs {y_analyte} fig1 raw.pdf',bbox_inches='tight')
        fig2.savefig(str(file)+ f' {x_analyte} vs {y_analyte} fig2 ratio.pdf',bbox_inches='tight')
        fig3.savefig(str(file)+ f' {x_analyte} vs {y_analyte} fig3 zscore.pdf',bbox_inches='tight')
        fig4.savefig(str(file)+ f' {x_analyte} vs {y_analyte} fig4 zrmean.pdf',bbox_inches='tight')
        fig5.savefig(str(file)+ f' {x_analyte} vs {y_analyte} fig5 diff.pdf',bbox_inches='tight')
        fig6.savefig(str(file)+ f' {x_analyte} vs {y_analyte} fig6 acorr.pdf',bbox_inches='tight')
        fig7.savefig(str(file)+ f' {x_analyte} vs {y_analyte} fig7 tlcc.pdf',bbox_inches='tight')
        fig8.savefig(str(file)+ f' {x_analyte} vs {y_analyte} fig8 wtlcc.pdf',bbox_inches='tight')
        fig9.savefig(str(file)+ f' {x_analyte} vs {y_analyte} fig9 rwtlcc.pdf',bbox_inches='tight')
        fig10.savefig(str(file)+ f' {x_analyte} vs {y_analyte} fig10 ips.pdf',bbox_inches='tight')

        with open('Output_'+str(file)+'_'+x_analyte+'_'+y_analyte+'.txt', 'a') as f:
            print('\n', file=f)
            print('----------------------------', file=f)
            print('Source: '+str(file), file=f)
            print('----------------------------', file=f)
            print('\n', file=f)
            print('AUC for: '+x_analyte+' vs '+y_analyte, file=f)
            print(f'AUC {x_analyte} = {y1_auc}', file=f)
            print(f'AUC {y_analyte} = {y2_auc}', file=f)
            print(f'AUC ratio {x_analyte}/{y_analyte} = {auc_ratio}', file=f)
            print('\n', file=f)
            print('Zscore ratios for: '+x_analyte+' vs '+y_analyte, file=f)
            print(df_zratio.describe(), file=f)
            print('\n', file=f)
            print('Spearman ranks', file=f)
            print(f'Spearman r: {r} and p-value: {p}', file=f)
            print('\n', file=f)
            print('Spearman correlation 1st-O diff:', file=f)
            print(f'{sc1d}', file=f)
            print('\n', file=f)

    os.chdir(data_path)

print("Group Simpson's AUC for",x_analyte,":",round(np.mean(y1_AUC),2),"+/-",round(np.std(y1_AUC),2))
print("Group Simpson's AUC for",y_analyte,":",round(np.mean(y2_AUC),2),"+/-",round(np.std(y2_AUC),2))

print()
print('DONE!')
