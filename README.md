# Dynamic interaction of systemic inflammation and the HPA axis during and after cardiac surgery

Codes and raw data associated to the paper https://royalsocietypublishing.org/doi/10.1098/rsif.2021.0925 

**Python 3.6 codes for non-stationary statistical analysis of ACTH and cortisol time series**

The scripts plot plasma ACTH and cortisol profiles from 3 healthy individuals and 10 CABG patients. It includes time-varying ACTH to cortisol ratios and their dynamic range, z-score normalisations and their time-varying Pearson correlation, ACTH to cortisol lag as revealed by the time-lagged cross-correlation (TLCC), phase synchrony revealed by the rolled window TLCC (RWTLCC) heatmaps, time-lagged autocorrelation, time-varying angle and instantaneous phase synchrony (IPS) between ACTH and cortisol, and some Fourier analysis of frequencies.

In order to run these scripts you will need to adjust the path and place the data files in separate folders for healthy controls and CABG patients.
